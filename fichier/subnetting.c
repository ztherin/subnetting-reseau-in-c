#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int n_bit( int n_subnet ) ;
int *subnet_mask( int n_bit , int reseau_IP[] , int *n_1 , int *n_2 ) ;
int intervall_subnet( int n ) ;

int main(int argc, char const *argv[])
{
	int i = 0 ;
	int j = 0 ;
	int k = 0 ;
	int n_1 = 0 ;
	int n_2 = 0 ;
	int intervall = 0 ;
	int cpt_subnet = 1 ;
	int cpt_intervall = 0 ;
	int n_subnet = 0 ;
	int reseau_IP[ 4 ] ;
	
	printf( "Veullez proceder a une entree octet par octet de l'adresse IPV4 de votre reseau : \n" ) ;
	
	printf( "   Octet 1 : " ) ;
	scanf( "%d" , &reseau_IP[ 0 ] ) ;

	printf( "   Octet 2 : " ) ;
	scanf( "%d" , &reseau_IP[ 1 ] ) ;

	printf( "   Octet 3 : " ) ;
	scanf( "%d" , &reseau_IP[ 2 ] ) ;

	printf( "   Octet 4 : " ) ;
	scanf( "%d" , &reseau_IP[ 3 ] ) ;

	printf( "En combien de sous-reseau voulez-vous le subdivise ? : " ) ;
	scanf( "%d" , &n_subnet ) ;
	
	int bits_to_masked = n_bit( n_subnet ) ;
	
	int *mask = NULL ;
	mask = subnet_mask( bits_to_masked , reseau_IP , &n_1 , &n_2 ) ;
	
	printf( "Le masque de sous-reseau est : " ) ;
	for( i = 0 ; i < 4 ; i++ )
	{
		printf( "%d" , mask[ i ] ) ;
		( i != 3 ) ? printf( "." ) : printf( "\n" ) ;
	}

	intervall = intervall_subnet( n_1 ) ;

	if( 4 - n_2 == 3 )
	{
		do
		{
			printf( "sous-reseau No : %d.....    %d.%d.%d.%d\n\n" , cpt_subnet , reseau_IP[ 0 ] , reseau_IP[ 1 ] + cpt_intervall , reseau_IP[ 2 ] , reseau_IP[ 3 ] ) ;
			printf( "..........Premier hote : %d.%d.%d.%d\n" , reseau_IP[ 0 ] , cpt_intervall , reseau_IP[ 2 ] , reseau_IP[ 3 ] + 1 ) ;
			printf( "..........Dernier hote : %d.%d.%d.%d\n" , reseau_IP[ 0 ] , cpt_intervall + intervall - 1 , 255 , 254 ) ;
			printf( "..........Broadcast Resau : %d.%d.%d.%d\n\n" , reseau_IP[ 0 ] , cpt_intervall + intervall - 1 , 255 , 255 ) ;			
			/*for( i = reseau_IP[ 1 ] + cpt_intervall ; i < cpt_intervall + intervall ; i++ )
			{
				for( j = 0 ; j < 256 ; j ++ )
				{
					for( k = 1 ; k < 255 ; k ++ )
					{
						printf( "..........Hote No %d : %d.%d.%d.%d\n" , cpt_host , reseau_IP[ 0 ] , i , j , k ) ;
						cpt_host++ ;
					}
				}
			}*/
			cpt_subnet ++ ;
			cpt_intervall += intervall ;
		}while( cpt_subnet < pow( 2 , bits_to_masked ) ) ;
	}

	else if( 4 - n_2 == 2 )
	{
		do
		{
			printf( "sous-reseau No : %d.....    %d.%d.%d.%d\n\n" , cpt_subnet , reseau_IP[ 0 ] , reseau_IP[ 1 ] , reseau_IP[ 2 ] + cpt_intervall , reseau_IP[ 3 ] ) ;
			printf( "..........Premier hote : %d.%d.%d.%d\n" , reseau_IP[ 0 ] , reseau_IP[ 1 ] , cpt_intervall , reseau_IP[ 3 ] + 1 ) ;
			printf( "..........Dernier hote : %d.%d.%d.%d\n" , reseau_IP[ 0 ] , reseau_IP[ 1 ] , cpt_intervall + intervall - 1 , 254 ) ;
			printf( "..........Broadcast Resau : %d.%d.%d.%d\n\n" , reseau_IP[ 0 ] ,  reseau_IP[ 1 ] , cpt_intervall + intervall - 1 , 255 ) ;			
			cpt_subnet ++ ;
			cpt_intervall += intervall ;
		}while( cpt_subnet < pow( 2 , bits_to_masked ) ) ;
	}

	else if( 4 - n_2 == 1 )
	{
		do
		{
			printf( "sous-reseau No : %d.....    %d.%d.%d.%d\n\n" , cpt_subnet , reseau_IP[ 0 ] , reseau_IP[ 1 ] , reseau_IP[ 2 ] , reseau_IP[ 3 ] +  cpt_intervall ) ;
			printf( "..........Premier hote : %d.%d.%d.%d\n" , reseau_IP[ 0 ] , reseau_IP[ 1 ] , reseau_IP[ 2 ] , cpt_intervall + 1 ) ;
			printf( "..........Dernier hote : %d.%d.%d.%d\n" , reseau_IP[ 0 ] , reseau_IP[ 1 ] , reseau_IP[ 2 ] , cpt_intervall + intervall - 1 ) ;
			printf( "..........Broadcast Resau : %d.%d.%d.%d\n\n" , reseau_IP[ 0 ] ,  reseau_IP[ 1 ] , reseau_IP[ 2 ] , cpt_intervall + intervall ) ;			
			cpt_subnet ++ ;
			cpt_intervall += intervall ;
		}while( cpt_subnet < pow( 2 , bits_to_masked ) ) ;
	}
	
	return 0 ;
}

int n_bit( int n_subnet )
{
	int i = 0 ;
	
	if( n_subnet <= 255 )
	{
		for( i = 0 ; i < 8 ; i ++  )
		{
			if( ( pow( 2 , i ) - 1 ) > n_subnet || ( pow( 2 , i ) - 1 ) == n_subnet )
				break ;
		}
	}

	else if( n_subnet > 255 )
	{
		for( i = 0 ; i < 100 ; i ++  )
		{
			if( ( pow( 2 , i ) - 1 ) > n_subnet || ( pow( 2 , i ) - 1 ) == n_subnet )
				break ;
		}	
	}
	
	return i ;
}

int *subnet_mask( int n_bit , int reseau_IP[] , int *n_1 , int *n_2 )
{
	int i = 0 ;
	int n = 0 ;
	int ctp = 0 ;
	int ctn = 0 ;
	int * pointer_mask = NULL ;
	
	if( n_bit <= 8 )
	{

		for( i = 7 ; i > -1 ; i -- )
		{
			ctp ++ ;
			( ctp > n_bit ) ? i = -1 : ( n += pow( 2 , i ) ) ;
		}

		*n_1 = n ;

		i = 3 ;
		ctp = 0 ;
		
		do
		{
			ctn ++ ;
			( reseau_IP[ i ] == 0 ) ? i-- : ctp++ ;
		
		}while( ctp == 0 && i != -1 ) ;
		
		if( ctn > 1 )
			i++ ; 

		*n_2 = i ;

		int mask[ 4 ] ;
		for( ctp = 0 ; ctp < 4 ; ctp ++ )
		{
			if( ctp < i )
				mask[ ctp ] = 255 ;
			else if( ctp == i )
				mask[ ctp ] = n ;
			else if( ctp > i )
				mask[ ctp ] = 0 ;
		}

		pointer_mask = mask ;
	}

	return pointer_mask ;

}

int intervall_subnet( int n )
{
	return 256 - n ;
}